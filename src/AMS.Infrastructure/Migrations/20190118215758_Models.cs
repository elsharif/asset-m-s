﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AMS.Infrastructure.Migrations
{
    public partial class Models : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AssetRelationTypes",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 75, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssetRelationTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AssetRoleTypes",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 75, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssetRoleTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AssetTypes",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 70, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssetTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ClassificationTypes",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 70, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClassificationTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ConfigurableEntities",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Discriminator = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConfigurableEntities", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FieldSets",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 70, nullable: false),
                    Description = table.Column<string>(maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FieldSets", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LocationTypes",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 70, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LocationTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OrganizationTypes",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 75, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrganizationTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PartyRelationTypes",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 75, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PartyRelationTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PartyRoleTypes",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 75, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PartyRoleTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AssetRelations",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    TypeId = table.Column<Guid>(nullable: false),
                    FromDate = table.Column<DateTime>(nullable: false, defaultValueSql: "getdate()"),
                    ThruDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssetRelations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AssetRelations_AssetRelationTypes_TypeId",
                        column: x => x.TypeId,
                        principalTable: "AssetRelationTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Assets",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    TypeId = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 70, nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Assets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Assets_AssetTypes_TypeId",
                        column: x => x.TypeId,
                        principalTable: "AssetTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ClassificationTypeValues",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    TypeId = table.Column<Guid>(nullable: false),
                    Value = table.Column<string>(maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClassificationTypeValues", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ClassificationTypeValues_ClassificationTypes_TypeId",
                        column: x => x.TypeId,
                        principalTable: "ClassificationTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AssetTypeFieldSets",
                columns: table => new
                {
                    TypeId = table.Column<Guid>(nullable: false),
                    FieldSetId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssetTypeFieldSets", x => new { x.TypeId, x.FieldSetId });
                    table.ForeignKey(
                        name: "FK_AssetTypeFieldSets_FieldSets_FieldSetId",
                        column: x => x.FieldSetId,
                        principalTable: "FieldSets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AssetTypeFieldSets_AssetTypes_TypeId",
                        column: x => x.TypeId,
                        principalTable: "AssetTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EntityFieldSets",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    FieldSetId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EntityFieldSets", x => new { x.EntityId, x.FieldSetId });
                    table.ForeignKey(
                        name: "FK_EntityFieldSets_ConfigurableEntities_EntityId",
                        column: x => x.EntityId,
                        principalTable: "ConfigurableEntities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EntityFieldSets_FieldSets_FieldSetId",
                        column: x => x.FieldSetId,
                        principalTable: "FieldSets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Fields",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    FieldSetId = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 70, nullable: false),
                    Type = table.Column<int>(nullable: false),
                    IsRequired = table.Column<bool>(nullable: false),
                    DefaultValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Fields", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Fields_FieldSets_FieldSetId",
                        column: x => x.FieldSetId,
                        principalTable: "FieldSets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Locations",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    TypeId = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 70, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Locations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Locations_LocationTypes_TypeId",
                        column: x => x.TypeId,
                        principalTable: "LocationTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Parties",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    DisplayName = table.Column<string>(nullable: true),
                    Discriminator = table.Column<string>(nullable: false),
                    Name = table.Column<string>(maxLength: 75, nullable: true),
                    TypeId = table.Column<Guid>(nullable: true),
                    FirstName = table.Column<string>(maxLength: 75, nullable: true),
                    MiddleName = table.Column<string>(maxLength: 75, nullable: true),
                    LastName = table.Column<string>(maxLength: 75, nullable: true),
                    Gender = table.Column<int>(nullable: true),
                    Birthdate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Parties", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Parties_OrganizationTypes_TypeId",
                        column: x => x.TypeId,
                        principalTable: "OrganizationTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "PartyRelations",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    TypeId = table.Column<Guid>(nullable: false),
                    FromDate = table.Column<DateTime>(nullable: false, defaultValueSql: "getdate()"),
                    ThruDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PartyRelations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PartyRelations_PartyRelationTypes_TypeId",
                        column: x => x.TypeId,
                        principalTable: "PartyRelationTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AssetRoles",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    TypeId = table.Column<Guid>(nullable: false),
                    AssetId = table.Column<Guid>(nullable: false),
                    FromDate = table.Column<DateTime>(nullable: false, defaultValueSql: "getdate()"),
                    ThruDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssetRoles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AssetRoles_Assets_AssetId",
                        column: x => x.AssetId,
                        principalTable: "Assets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AssetRoles_ConfigurableEntities_Id",
                        column: x => x.Id,
                        principalTable: "ConfigurableEntities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AssetRoles_AssetRoleTypes_TypeId",
                        column: x => x.TypeId,
                        principalTable: "AssetRoleTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AssetClassifications",
                columns: table => new
                {
                    AssetId = table.Column<Guid>(nullable: false),
                    ClassificationId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssetClassifications", x => new { x.AssetId, x.ClassificationId });
                    table.ForeignKey(
                        name: "FK_AssetClassifications_Assets_AssetId",
                        column: x => x.AssetId,
                        principalTable: "Assets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AssetClassifications_ClassificationTypeValues_ClassificationId",
                        column: x => x.ClassificationId,
                        principalTable: "ClassificationTypeValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FieldValues",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(nullable: false),
                    FieldId = table.Column<Guid>(nullable: false),
                    Value = table.Column<string>(nullable: true),
                    LastUpdate = table.Column<DateTime>(nullable: false, defaultValueSql: "getdate()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FieldValues", x => new { x.EntityId, x.FieldId });
                    table.ForeignKey(
                        name: "FK_FieldValues_ConfigurableEntities_EntityId",
                        column: x => x.EntityId,
                        principalTable: "ConfigurableEntities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FieldValues_Fields_FieldId",
                        column: x => x.FieldId,
                        principalTable: "Fields",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PartyAddresses",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    PartyId = table.Column<Guid>(nullable: false),
                    Address_Name = table.Column<string>(maxLength: 20, nullable: false),
                    Address_Country = table.Column<string>(maxLength: 50, nullable: true),
                    Address_City = table.Column<string>(maxLength: 50, nullable: true),
                    Address_Line1 = table.Column<string>(maxLength: 100, nullable: true),
                    Address_Line2 = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PartyAddresses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PartyAddresses_Parties_PartyId",
                        column: x => x.PartyId,
                        principalTable: "Parties",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PartyClassifications",
                columns: table => new
                {
                    PartyId = table.Column<Guid>(nullable: false),
                    ClassificationId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PartyClassifications", x => new { x.PartyId, x.ClassificationId });
                    table.ForeignKey(
                        name: "FK_PartyClassifications_ClassificationTypeValues_ClassificationId",
                        column: x => x.ClassificationId,
                        principalTable: "ClassificationTypeValues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PartyClassifications_Parties_PartyId",
                        column: x => x.PartyId,
                        principalTable: "Parties",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PartyEmailAddresses",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    PartyId = table.Column<Guid>(nullable: false),
                    EmailAddress_Name = table.Column<string>(maxLength: 20, nullable: false),
                    EmailAddress_Value = table.Column<string>(maxLength: 250, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PartyEmailAddresses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PartyEmailAddresses_Parties_PartyId",
                        column: x => x.PartyId,
                        principalTable: "Parties",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PartyPhoneNumbers",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    PartyId = table.Column<Guid>(nullable: false),
                    PhoneNumber_Name = table.Column<string>(maxLength: 20, nullable: false),
                    PhoneNumber_Number = table.Column<string>(maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PartyPhoneNumbers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PartyPhoneNumbers_Parties_PartyId",
                        column: x => x.PartyId,
                        principalTable: "Parties",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PartyRoles",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    TypeId = table.Column<Guid>(nullable: false),
                    PartyId = table.Column<Guid>(nullable: false),
                    FromDate = table.Column<DateTime>(nullable: false, defaultValueSql: "getdate()"),
                    ThruDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PartyRoles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PartyRoles_ConfigurableEntities_Id",
                        column: x => x.Id,
                        principalTable: "ConfigurableEntities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PartyRoles_Parties_PartyId",
                        column: x => x.PartyId,
                        principalTable: "Parties",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PartyRoles_PartyRoleTypes_TypeId",
                        column: x => x.TypeId,
                        principalTable: "PartyRoleTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AssetRelationRoles",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RelationId = table.Column<Guid>(nullable: false),
                    RoleId = table.Column<Guid>(nullable: false),
                    RoleName = table.Column<string>(maxLength: 70, nullable: false),
                    FromDate = table.Column<DateTime>(nullable: false, defaultValueSql: "getdate()"),
                    ThruDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssetRelationRoles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AssetRelationRoles_AssetRelations_RelationId",
                        column: x => x.RelationId,
                        principalTable: "AssetRelations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AssetRelationRoles_AssetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AssetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AssetPartyRelationRoles",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RelationId = table.Column<Guid>(nullable: false),
                    RoleId = table.Column<Guid>(nullable: false),
                    RoleName = table.Column<string>(maxLength: 70, nullable: false),
                    FromDate = table.Column<DateTime>(nullable: false, defaultValueSql: "getdate()"),
                    ThruDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssetPartyRelationRoles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AssetPartyRelationRoles_AssetRelations_RelationId",
                        column: x => x.RelationId,
                        principalTable: "AssetRelations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AssetPartyRelationRoles_PartyRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "PartyRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PartyRelationRoles",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RelationId = table.Column<Guid>(nullable: false),
                    RoleId = table.Column<Guid>(nullable: false),
                    RoleName = table.Column<string>(maxLength: 70, nullable: false),
                    FromDate = table.Column<DateTime>(nullable: false, defaultValueSql: "getdate()"),
                    ThruDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PartyRelationRoles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PartyRelationRoles_PartyRelations_RelationId",
                        column: x => x.RelationId,
                        principalTable: "PartyRelations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PartyRelationRoles_PartyRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "PartyRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AssetClassifications_ClassificationId",
                table: "AssetClassifications",
                column: "ClassificationId");

            migrationBuilder.CreateIndex(
                name: "IX_AssetPartyRelationRoles_RoleId",
                table: "AssetPartyRelationRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_AssetPartyRelationRole_RoleName",
                table: "AssetPartyRelationRoles",
                column: "RoleName");

            migrationBuilder.CreateIndex(
                name: "IX_AssetPartyRelationRole_Relation",
                table: "AssetPartyRelationRoles",
                columns: new[] { "RelationId", "RoleId", "RoleName" });

            migrationBuilder.CreateIndex(
                name: "IX_AssetRelationRoles_RoleId",
                table: "AssetRelationRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_AssetRelationRole_RoleName",
                table: "AssetRelationRoles",
                column: "RoleName");

            migrationBuilder.CreateIndex(
                name: "IX_AssetRelationRole_Relation",
                table: "AssetRelationRoles",
                columns: new[] { "RelationId", "RoleId", "RoleName" });

            migrationBuilder.CreateIndex(
                name: "IX_AssetRelation_Type",
                table: "AssetRelations",
                column: "TypeId");

            migrationBuilder.CreateIndex(
                name: "IX_AssetRelationType_Name",
                table: "AssetRelationTypes",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_AssetRoles_TypeId",
                table: "AssetRoles",
                column: "TypeId");

            migrationBuilder.CreateIndex(
                name: "IX_AssetRole_Asset_Type",
                table: "AssetRoles",
                columns: new[] { "AssetId", "TypeId" });

            migrationBuilder.CreateIndex(
                name: "IX_AssetRoleType_Name",
                table: "AssetRoleTypes",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_Asset_Name",
                table: "Assets",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_Assets_TypeId",
                table: "Assets",
                column: "TypeId");

            migrationBuilder.CreateIndex(
                name: "IX_AssetTypeFieldSets_FieldSetId",
                table: "AssetTypeFieldSets",
                column: "FieldSetId");

            migrationBuilder.CreateIndex(
                name: "IX_AssetType_Name",
                table: "AssetTypes",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_ClassificationType_Name",
                table: "ClassificationTypes",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_ClassificationTypeValues_TypeId",
                table: "ClassificationTypeValues",
                column: "TypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassificationTypeValue_Value",
                table: "ClassificationTypeValues",
                column: "Value");

            migrationBuilder.CreateIndex(
                name: "IX_EntityFieldSets_FieldSetId",
                table: "EntityFieldSets",
                column: "FieldSetId");

            migrationBuilder.CreateIndex(
                name: "IX_FieldSet_Name",
                table: "Fields",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_FieldSet_Field_Name",
                table: "Fields",
                columns: new[] { "FieldSetId", "Name" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_FieldSet_Name",
                table: "FieldSets",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_FieldValues_FieldId",
                table: "FieldValues",
                column: "FieldId");

            migrationBuilder.CreateIndex(
                name: "IX_Location_Name",
                table: "Locations",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_Location_Type_Name",
                table: "Locations",
                columns: new[] { "TypeId", "Name" });

            migrationBuilder.CreateIndex(
                name: "IX_LocationType_Name",
                table: "LocationTypes",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_OrganizationType_Name",
                table: "OrganizationTypes",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Organization_Name",
                table: "Parties",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_Parties_TypeId",
                table: "Parties",
                column: "TypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Person_LastName",
                table: "Parties",
                column: "LastName");

            migrationBuilder.CreateIndex(
                name: "IX_PartyAddresses_PartyId",
                table: "PartyAddresses",
                column: "PartyId");

            migrationBuilder.CreateIndex(
                name: "IX_PartyClassifications_ClassificationId",
                table: "PartyClassifications",
                column: "ClassificationId");

            migrationBuilder.CreateIndex(
                name: "IX_PartyEmailAddresses_PartyId",
                table: "PartyEmailAddresses",
                column: "PartyId");

            migrationBuilder.CreateIndex(
                name: "IX_PartyPhoneNumbers_PartyId",
                table: "PartyPhoneNumbers",
                column: "PartyId");

            migrationBuilder.CreateIndex(
                name: "IX_PartyRelationRoles_RoleId",
                table: "PartyRelationRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_PartyRelationRole_RoleName",
                table: "PartyRelationRoles",
                column: "RoleName");

            migrationBuilder.CreateIndex(
                name: "IX_PartyRelationRole_Relation",
                table: "PartyRelationRoles",
                columns: new[] { "RelationId", "RoleId", "RoleName" });

            migrationBuilder.CreateIndex(
                name: "IX_PartyRelation_Type",
                table: "PartyRelations",
                column: "TypeId");

            migrationBuilder.CreateIndex(
                name: "IX_PartyRelationType_Name",
                table: "PartyRelationTypes",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_PartyRoles_TypeId",
                table: "PartyRoles",
                column: "TypeId");

            migrationBuilder.CreateIndex(
                name: "IX_PartyRole_Party_Type",
                table: "PartyRoles",
                columns: new[] { "PartyId", "TypeId" });

            migrationBuilder.CreateIndex(
                name: "IX_PartyRoleType_Name",
                table: "PartyRoleTypes",
                column: "Name");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AssetClassifications");

            migrationBuilder.DropTable(
                name: "AssetPartyRelationRoles");

            migrationBuilder.DropTable(
                name: "AssetRelationRoles");

            migrationBuilder.DropTable(
                name: "AssetTypeFieldSets");

            migrationBuilder.DropTable(
                name: "EntityFieldSets");

            migrationBuilder.DropTable(
                name: "FieldValues");

            migrationBuilder.DropTable(
                name: "Locations");

            migrationBuilder.DropTable(
                name: "PartyAddresses");

            migrationBuilder.DropTable(
                name: "PartyClassifications");

            migrationBuilder.DropTable(
                name: "PartyEmailAddresses");

            migrationBuilder.DropTable(
                name: "PartyPhoneNumbers");

            migrationBuilder.DropTable(
                name: "PartyRelationRoles");

            migrationBuilder.DropTable(
                name: "AssetRelations");

            migrationBuilder.DropTable(
                name: "AssetRoles");

            migrationBuilder.DropTable(
                name: "Fields");

            migrationBuilder.DropTable(
                name: "LocationTypes");

            migrationBuilder.DropTable(
                name: "ClassificationTypeValues");

            migrationBuilder.DropTable(
                name: "PartyRelations");

            migrationBuilder.DropTable(
                name: "PartyRoles");

            migrationBuilder.DropTable(
                name: "AssetRelationTypes");

            migrationBuilder.DropTable(
                name: "Assets");

            migrationBuilder.DropTable(
                name: "AssetRoleTypes");

            migrationBuilder.DropTable(
                name: "FieldSets");

            migrationBuilder.DropTable(
                name: "ClassificationTypes");

            migrationBuilder.DropTable(
                name: "PartyRelationTypes");

            migrationBuilder.DropTable(
                name: "ConfigurableEntities");

            migrationBuilder.DropTable(
                name: "Parties");

            migrationBuilder.DropTable(
                name: "PartyRoleTypes");

            migrationBuilder.DropTable(
                name: "AssetTypes");

            migrationBuilder.DropTable(
                name: "OrganizationTypes");
        }
    }
}
