﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AMS.Infrastructure.Models
{
    public class Client
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public string Name { get; set; }
        public List<DefaultAsset> Assets { get; private set; } = new List<DefaultAsset>();
    }
}
