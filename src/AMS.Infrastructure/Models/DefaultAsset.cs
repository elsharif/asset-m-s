﻿using Shared.AssetModels;
using System;
using System.Collections.Generic;

namespace AMS.Infrastructure.Models
{
    public class DefaultAsset
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public AssetType Type { get; set; }
        public Client Client { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Dictionary<string, string> Values { get; private set; } = new Dictionary<string, string>();
    }
}
