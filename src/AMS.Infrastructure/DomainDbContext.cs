﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Shared.PartyModels;
using Shared.PartyModels.EntityFrameworkCore;
using Shared.Models;
using Shared.Models.EntityFrameworkCore;
using Shared.MetaModels;
using Shared.MetaModels.EntityFrameworkCore;
using Shared.AssetModels;
using Shared.AssetModels.EntityFrameworkCore;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using AMS.Infrastructure.Models;
using System.Linq;
using Shared.DocumentModels;
using Shared.DocumentModels.EntityFrameworkCore;

namespace AMS.Infrastructure
{
    public class DomainDbContext : DbContext
    {
        public const string RT_P_CLIENT = "Client";
        public const string RT_P_USER = "User";
        public const string RT_P_CUSTODIAN = "Asset Custodian";
        public const string RT_A_DEFAULT = "Default";
        public const string RET_P_CLIENT_USER = "Client User";
        public const string RET_A_CLIENT_ASSET = "Client Asset";

        public static ConcurrentDictionary<string, PartyRoleType> CurrentPartyRoleTypes = new ConcurrentDictionary<string, PartyRoleType>();
        public static ConcurrentDictionary<string, PartyRelationType> CurrentPartyRelationTypes = new ConcurrentDictionary<string, PartyRelationType>();
        public static ConcurrentDictionary<string, AssetRoleType> CurrentAssetRoleTypes = new ConcurrentDictionary<string, AssetRoleType>();
        public static ConcurrentDictionary<string, AssetRelationType> CurrentAssetRelationTypes = new ConcurrentDictionary<string, AssetRelationType>();

        public DomainDbContext(DbContextOptions options)
            :base(options)
        {
        }

        #region Models
        public DbSet<ConfigurableEntity> ConfigurableEntities { get; set; }
        public DbSet<FieldSet> FieldSets { get; set; }
        public DbSet<Field> Fields { get; set; }
        public DbSet<FieldValue> FieldValues { get; set; }
        public DbSet<EntityFieldSet> EntityFieldSets { get; set; }

        public DbSet<ClassificationType> ClassificationTypes { get; set; }
        public DbSet<ClassificationTypeValue> ClassificationTypeValues { get; set; }
        public DbSet<LocationType> LocationTypes { get; set; }
        public DbSet<Location> Locations { get; set; }

        public DbSet<Party> Parties { get; set; }
        public DbSet<Organization> Organizations { get; set; }
        public DbSet<Person> People { get; set; }
        public DbSet<OrganizationType> OrganizationTypes { get; set; }
        public DbSet<PartyRoleType> PartyRoleTypes { get; set; }
        public DbSet<PartyRelationType> PartyRelationTypes { get; set; }
        public DbSet<PartyRole> PartyRoles { get; set; }
        public DbSet<PartyRelation> PartyRelations { get; set; }
        public DbSet<PartyRelationRole> PartyRelationRoles { get; set; }
        public DbSet<PartyRoleConfiguration> PartyRoleConfigurations { get; set; }
        public DbSet<PartyClassification> PartyClassifications { get; set; }
        public DbSet<PartyPhoneNumber> PartyPhoneNumbers { get; set; }
        public DbSet<PartyEmailAddress> PartyEmailAddresses { get; set; }
        public DbSet<PartyAddress> PartyAddresses { get; set; }

        public DbSet<AssetType> AssetTypes { get; set; }
        public DbSet<Asset> Assets { get; set; }
        public DbSet<AssetRoleType> AssetRoleTypes { get; set; }
        public DbSet<AssetRelationType> AssetRelationTypes { get; set; }
        public DbSet<AssetRole> AssetRoles { get; set; }
        public DbSet<AssetRelation> AssetRelations { get; set; }
        public DbSet<AssetRelationRole> AssetRelationRoles { get; set; }
        public DbSet<AssetPartyRelationRole> AssetPartyRelationRoles { get; set; }
        public DbSet<AssetRoleConfiguration> AssetRoleConfigurations { get; set; }
        public DbSet<AssetTypeFieldSet> AssetTypeFieldSets { get; set; }
        public DbSet<AssetClassification> AssetClassifications { get; set; }

        public DbSet<Repository> Repositories { get; set; }
        public DbSet<Folder> Folders { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<DocumentFile> DocumentFiles { get; set; }
        public DbSet<FileChunk> FileChunks { get; set; }
        #endregion

        public async Task<List<Client>> GetClients()
        {
            var clients = await this.PartyRoles
                .Include(x => x.Party)
                .Where(pr => pr.TypeId == CurrentPartyRoleTypes[RT_P_CLIENT].Id)
                .Select(x => new Client { Id = x.Id, Name = x.Party.DisplayName })
                .ToListAsync();
            return clients;
        }

        public async Task<Client> GetClientById(Guid id)
        {
            var client = await this.PartyRoles
                .Include(x => x.Party)
                .Where(pr => pr.Id == id && pr.TypeId == CurrentPartyRoleTypes[RT_P_CLIENT].Id)
                .Select(x => new Client { Id = x.Id, Name = x.Party.DisplayName })
                .FirstOrDefaultAsync();
            return client;
        }

        public async Task<Client> AddClient(Client client)
        {
            var org = new Organization { Id = Guid.NewGuid(), DisplayName = client.Name, Name = client.Name };
            var role = new PartyRole { Id = Guid.NewGuid(), Party = org, FromDate = DateTime.Now, TypeId = CurrentPartyRoleTypes[RT_P_CLIENT].Id };
            this.PartyRoles.Add(role);
            await SaveChangesAsync();
            client.Id = role.Id;
            return client;
        }

        public async Task<Client> UpdateClient(Client client)
        {
            var role = await this.PartyRoles
                .Include(x => x.Party)
                .Where(pr => pr.Id == client.Id && pr.TypeId == CurrentPartyRoleTypes[RT_P_CLIENT].Id)
                .FirstOrDefaultAsync();
            (role.Party as Organization).Name = role.Party.DisplayName = client.Name;
            await SaveChangesAsync();
            return client;
        }

        public async Task<List<DefaultAsset>> GetAssets(Guid clientId)
        {
            var client = await GetClientById(clientId);
            var relations = await this.AssetRelations
                .Include(x => x.Roles).ThenInclude(x => x.Role).ThenInclude(x => x.Asset).ThenInclude(x => x.Type)
                .Include(x => x.PartyRoles)
                .Where(x => x.TypeId == CurrentAssetRelationTypes[RET_A_CLIENT_ASSET].Id && x.PartyRoles.Any(p => p.RoleId == clientId))
                .ToListAsync();
            var assets = new List<DefaultAsset>();
            foreach (var obj in relations)
            {
                var asset = new DefaultAsset
                {
                    Id = obj.Roles.First().RoleId,
                    Name = obj.Roles.First().Role.Asset.Name,
                    Description = obj.Roles.First().Role.Asset.Description,
                    Type = obj.Roles.First().Role.Asset.Type,
                    Client = client
                };
                assets.Add(asset);
            }
            return assets;
        }

        public async Task<DefaultAsset> GetAssetById(Guid assetId, Guid clientId)
        {
            var obj = await this.AssetRelations
                .Include(x => x.Roles).ThenInclude(x => x.Role).ThenInclude(x => x.Asset).ThenInclude(x => x.Type)
                .Include(x => x.PartyRoles).ThenInclude(x => x.Role).ThenInclude(x => x.Party)
                .Where(x => x.TypeId == CurrentAssetRelationTypes[RET_A_CLIENT_ASSET].Id && x.PartyRoles.Any(p => p.RoleId == clientId) && x.Roles.Any(a => a.RoleId == assetId))
                .FirstOrDefaultAsync();

            var asset = new DefaultAsset
            {
                Id = obj.Roles.First().RoleId,
                Name = obj.Roles.First().Role.Asset.Name,
                Description = obj.Roles.First().Role.Asset.Description,
                Type = obj.Roles.First().Role.Asset.Type,
                Client = obj.PartyRoles.Select(x => new Client { Id = x.Role.Id, Name = x.Role.Party.DisplayName }).First()
            };
            return asset;
        }

        public async Task<DefaultAsset> AddAsset(DefaultAsset asset)
        {
            var role = await this.PartyRoles
                .Include(x => x.Party)
                .Where(pr => pr.Id == asset.Client.Id && pr.TypeId == CurrentPartyRoleTypes[RT_P_CLIENT].Id)
                .FirstOrDefaultAsync();
            var assetObj = new Asset { Id = Guid.NewGuid(), TypeId = asset.Type.Id, Name = asset.Name, Description = asset.Description };
            var assetRole = new AssetRole { Id = asset.Id, Asset = assetObj, FromDate = DateTime.Now, TypeId = CurrentAssetRoleTypes[RT_A_DEFAULT].Id };
            this.AssetRoles.Add(assetRole);

            var rel = new AssetRelation { Id = Guid.NewGuid(),  FromDate = DateTime.Now, TypeId = CurrentAssetRelationTypes[RET_A_CLIENT_ASSET].Id };
            rel.Roles.Add(new AssetRelationRole { Id = Guid.NewGuid(), Role = assetRole, FromDate = DateTime.Now, RoleName = CurrentAssetRoleTypes[RT_A_DEFAULT].Name });
            rel.PartyRoles.Add(new AssetPartyRelationRole { Id = Guid.NewGuid(), Role = role, FromDate = DateTime.Now, RoleName = CurrentPartyRoleTypes[RT_P_CLIENT].Name });
            this.AssetRelations.Add(rel);
            await SaveChangesAsync();
            asset.Id = assetRole.Id;
            return asset;
        }

        public async Task<DefaultAsset> UpdateAsset(DefaultAsset asset)
        {
            var obj = await this.AssetRelations
                .Include(x => x.Roles).ThenInclude(x => x.Role).ThenInclude(x => x.Asset).ThenInclude(x => x.Type)
                .Include(x => x.PartyRoles).ThenInclude(x => x.Role).ThenInclude(x => x.Party)
                .Where(x => x.TypeId == CurrentAssetRelationTypes[RET_A_CLIENT_ASSET].Id && x.PartyRoles.Any(p => p.RoleId == asset.Client.Id) && x.Roles.Any(a => a.RoleId == asset.Id))
                .FirstOrDefaultAsync();
            var assetEntity = obj.Roles.First().Role.Asset;
            assetEntity.Name = asset.Name;
            assetEntity.Description = asset.Description;
            assetEntity.TypeId = asset.Type.Id;
            this.Assets.Update(assetEntity);
            await this.SaveChangesAsync();
            return asset;
        }

        public async Task Initialize()
        {
            var partyRoleTypes = await this.PartyRoleTypes.ToListAsync();
            var assetRoleTypes = await this.AssetRoleTypes.ToListAsync();
            var partyRelationTypes = await this.PartyRelationTypes.ToListAsync();
            var assetRelationTypes = await this.AssetRelationTypes.ToListAsync();
            foreach (var obj in partyRoleTypes)
            {
                CurrentPartyRoleTypes[obj.Name] = obj;
            }
            foreach (var obj in partyRelationTypes)
            {
                CurrentPartyRelationTypes[obj.Name] = obj;
            }
            foreach (var obj in assetRoleTypes)
            {
                CurrentAssetRoleTypes[obj.Name] = obj;
            }
            foreach (var obj in AssetRelationTypes)
            {
                CurrentAssetRelationTypes[obj.Name] = obj;
            }

            var partyRoleTypeNames = new[] { RT_P_CLIENT, RT_P_USER, RT_P_CUSTODIAN };
            foreach(var name in partyRoleTypeNames)
            {
                if (!CurrentPartyRoleTypes.ContainsKey(name))
                {
                    var obj = new PartyRoleType { Id = Guid.NewGuid(), Name = name };
                    this.PartyRoleTypes.Add(obj);
                    await SaveChangesAsync();
                    CurrentPartyRoleTypes[obj.Name] = obj;
                }
            }

            var assetRoleTypeNames = new[] { RT_A_DEFAULT };
            foreach (var name in assetRoleTypeNames)
            {
                if (!CurrentAssetRoleTypes.ContainsKey(name))
                {
                    var obj = new AssetRoleType { Id = Guid.NewGuid(), Name = name };
                    this.AssetRoleTypes.Add(obj);
                    await SaveChangesAsync();
                    CurrentAssetRoleTypes[obj.Name] = obj;
                }
            }

            var partyRelationTypeNames = new[] { RET_P_CLIENT_USER };
            foreach (var name in partyRelationTypeNames)
            {
                if (!CurrentPartyRelationTypes.ContainsKey(name))
                {
                    var obj = new PartyRelationType { Id = Guid.NewGuid(), Name = name };
                    this.PartyRelationTypes.Add(obj);
                    await SaveChangesAsync();
                    CurrentPartyRelationTypes[obj.Name] = obj;
                }
            }

            var assetRelationTypeNames = new[] { RET_A_CLIENT_ASSET };
            foreach (var name in assetRelationTypeNames)
            {
                if (!CurrentAssetRelationTypes.ContainsKey(name))
                {
                    var obj = new AssetRelationType { Id = Guid.NewGuid(), Name = name };
                    this.AssetRelationTypes.Add(obj);
                    await SaveChangesAsync();
                    CurrentAssetRelationTypes[obj.Name] = obj;
                }
            }
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ApplyConfiguration(new FieldSetEntityTypeConfiguration());
            builder.ApplyConfiguration(new FieldEntityTypeConfiguration());
            builder.ApplyConfiguration(new FieldValueEntityTypeConfiguration());
            builder.ApplyConfiguration(new EntityFieldSetEntityTypeConfiguration());

            builder.ApplyConfiguration(new ClassificationTypeEntityTypeConfiguration());
            builder.ApplyConfiguration(new ClassificationTypeValueEntityTypeConfiguration());
            builder.ApplyConfiguration(new LocationTypeEntityTypeConfiguration());
            builder.ApplyConfiguration(new LocationEntityTypeConfiguration());

            builder.ApplyConfiguration(new OrganizationTypeEntityTypeConfiguration());
            builder.ApplyConfiguration(new OrganizationEntityTypeConfiguration());
            builder.ApplyConfiguration(new PersonEntityTypeConfiguration());
            builder.ApplyConfiguration(new PartyRoleTypeEntityTypeConfiguration());
            builder.ApplyConfiguration(new PartyRelationTypeEntityTypeConfiguration());
            builder.ApplyConfiguration(new PartyRoleEntityTypeConfiguration());
            builder.ApplyConfiguration(new PartyRelationEntityTypeConfiguration());
            builder.ApplyConfiguration(new PartyRelationRoleEntityTypeConfiguration());
            builder.ApplyConfiguration(new PartyRoleConfigurationEntityTypeConfiguration());
            builder.ApplyConfiguration(new PartyClassificationEntityTypeConfiguration());
            builder.ApplyConfiguration(new PartyPhoneNumberEntityTypeConfiguration());
            builder.ApplyConfiguration(new PartyEmailAddressEntityTypeConfiguration());
            builder.ApplyConfiguration(new PartyAddressEntityTypeConfiguration());

            builder.ApplyConfiguration(new AssetTypeEntityTypeConfiguration());
            builder.ApplyConfiguration(new AssetEntityTypeConfiguration());
            builder.ApplyConfiguration(new AssetRoleTypeEntityTypeConfiguration());
            builder.ApplyConfiguration(new AssetRelationTypeEntityTypeConfiguration());
            builder.ApplyConfiguration(new AssetRoleEntityTypeConfiguration());
            builder.ApplyConfiguration(new AssetRoleConfigurationEntityTypeConfiguration());
            builder.ApplyConfiguration(new AssetRelationEntityTypeConfiguration());
            builder.ApplyConfiguration(new AssetRelationRoleEntityTypeConfiguration());
            builder.ApplyConfiguration(new AssetPartyRelationRoleEntityTypeConfiguration());
            builder.ApplyConfiguration(new AssetTypeFieldSetEntityTypeConfiguration());
            builder.ApplyConfiguration(new AssetClassificationEntityTypeConfiguration());

            builder.ApplyConfiguration(new RepositoryEntityTypeConfiguration());
            builder.ApplyConfiguration(new FolderEntityTypeConfiguration());
            builder.ApplyConfiguration(new DocumentEntityTypeConfiguration());
            builder.ApplyConfiguration(new DocumentFileEntityTypeConfiguration());
            builder.ApplyConfiguration(new FileChunkEntityTypeConfiguration());
        }
    }
}
