﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.DocumentModels.EntityFrameworkCore
{
    public class RepositoryEntityTypeConfiguration : IEntityTypeConfiguration<Repository>
    {
        public void Configure(EntityTypeBuilder<Repository> builder)
        {
            builder.Property(p => p.Name).HasMaxLength(70).IsRequired();
        }
    }
}
