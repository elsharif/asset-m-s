﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Shared.DocumentModels.EntityFrameworkCore
{
    public class DocumentFileEntityTypeConfiguration : IEntityTypeConfiguration<DocumentFile>
    {
        public void Configure(EntityTypeBuilder<DocumentFile> builder)
        {
            builder.HasOne(p => p.Document)
               .WithMany(p => p.Files)
               .HasForeignKey(p => p.DocumentId)
               .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
