﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Shared.DocumentModels.EntityFrameworkCore
{
    public class FolderEntityTypeConfiguration : IEntityTypeConfiguration<Folder>
    {
        public void Configure(EntityTypeBuilder<Folder> builder)
        {
            builder.Property(p => p.Title).HasMaxLength(70).IsRequired();

            // Relations
            builder.HasOne(p => p.Repository)
                .WithMany(p => p.Folders)
                .HasForeignKey(p => p.RepositoryId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(p => p.Parent)
                .WithMany(p => p.Childs)
                .HasForeignKey(p => p.ParentId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
