﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Shared.DocumentModels.EntityFrameworkCore
{
    public class DocumentEntityTypeConfiguration : IEntityTypeConfiguration<Document>
    {
        public void Configure(EntityTypeBuilder<Document> builder)
        {
            builder.Property(p => p.Title).HasMaxLength(70).IsRequired();

            // Relations
            builder.HasOne(p => p.Repository)
                .WithMany(p => p.Documents)
                .HasForeignKey(p => p.RepositoryId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(p => p.Folder)
               .WithMany(p => p.Documents)
               .HasForeignKey(p => p.FolderId)
               .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
