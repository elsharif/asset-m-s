﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Shared.DocumentModels.EntityFrameworkCore
{
    public class FileChunkEntityTypeConfiguration : IEntityTypeConfiguration<FileChunk>
    {
        public void Configure(EntityTypeBuilder<FileChunk> builder)
        {
            builder.HasOne(p => p.DocumentFile)
               .WithMany(p => p.Chunks)
               .HasForeignKey(p => p.DocumentFileId)
               .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
