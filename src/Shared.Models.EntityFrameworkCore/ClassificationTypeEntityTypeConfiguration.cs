﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.Models.EntityFrameworkCore
{
    public class ClassificationTypeEntityTypeConfiguration : IEntityTypeConfiguration<ClassificationType>
    {
        public void Configure(EntityTypeBuilder<ClassificationType> builder)
        {
            builder.Property(p => p.Name).HasMaxLength(70).IsRequired();

            // Indexes
            builder.HasIndex(p => p.Name).HasName($"IX_{nameof(ClassificationType)}_{nameof(ClassificationType.Name)}");
        }
    }
}
