﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Shared.Models.EntityFrameworkCore
{
    public class ClassificationTypeValueEntityTypeConfiguration : IEntityTypeConfiguration<ClassificationTypeValue>
    {
        public void Configure(EntityTypeBuilder<ClassificationTypeValue> builder)
        {
            builder.Property(p => p.Value).HasMaxLength(30).IsRequired();

            // Relations
            builder.HasOne(p => p.Type)
                .WithMany(p => p.Values)
                .HasForeignKey(p => p.TypeId)
                .OnDelete(DeleteBehavior.Restrict);

            // Indexes
            builder.HasIndex(p => p.Value).HasName($"IX_{nameof(ClassificationTypeValue)}_{nameof(ClassificationTypeValue.Value)}");
        }
    }
}
