﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Shared.Models.EntityFrameworkCore
{
    public class LocationEntityTypeConfiguration : IEntityTypeConfiguration<Location>
    {
        public void Configure(EntityTypeBuilder<Location> builder)
        {
            builder.Property(p => p.Name).HasMaxLength(70).IsRequired();

            // Relations
            builder.HasOne(p => p.Type)
                .WithMany(p => p.Locations)
                .HasForeignKey(p => p.TypeId)
                .OnDelete(DeleteBehavior.Restrict);

            // Indexes
            builder.HasIndex(p => p.Name).HasName($"IX_{nameof(Location)}_{nameof(Location.Name)}");
            builder.HasIndex(p => new { p.TypeId, p.Name }).HasName($"IX_{nameof(Location)}_{nameof(Location.Type)}_{nameof(Location.Name)}");
        }
    }
}
