﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Shared.Models.EntityFrameworkCore
{
    public class LocationTypeEntityTypeConfiguration : IEntityTypeConfiguration<LocationType>
    {
        public void Configure(EntityTypeBuilder<LocationType> builder)
        {
            builder.Property(p => p.Name).HasMaxLength(70).IsRequired();

            // Indexes
            builder.HasIndex(p => p.Name).HasName($"IX_{nameof(LocationType)}_{nameof(LocationType.Name)}");
        }
    }
}
