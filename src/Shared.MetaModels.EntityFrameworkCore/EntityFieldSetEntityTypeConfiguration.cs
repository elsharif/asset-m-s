﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Shared.MetaModels.EntityFrameworkCore
{
    public class EntityFieldSetEntityTypeConfiguration : IEntityTypeConfiguration<EntityFieldSet>
    {
        public void Configure(EntityTypeBuilder<EntityFieldSet> builder)
        {
            builder.HasKey(p => new { p.EntityId, p.FieldSetId });

            // Relations
            builder.HasOne(p => p.FieldSet)
                .WithMany(p => p.Entities)
                .HasForeignKey(p => p.FieldSetId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(p => p.Entity)
                .WithMany(p => p.FieldSets)
                .HasForeignKey(p => p.EntityId)
                .OnDelete(DeleteBehavior.Restrict);

        }
    }
}
