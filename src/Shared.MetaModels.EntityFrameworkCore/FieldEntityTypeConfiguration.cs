﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Shared.MetaModels.EntityFrameworkCore
{
    public class FieldEntityTypeConfiguration : IEntityTypeConfiguration<Field>
    {
        public void Configure(EntityTypeBuilder<Field> builder)
        {
            builder.Property(p => p.Name).HasMaxLength(70).IsRequired();

            // Relations
            builder.HasOne(p => p.FieldSet)
                .WithMany(p => p.Fields)
                .HasForeignKey(p => p.FieldSetId)
                .OnDelete(DeleteBehavior.Restrict);

            // Indexes
            builder.HasIndex(p => p.Name).HasName($"IX_{nameof(FieldSet)}_{nameof(FieldSet.Name)}");
            builder.HasIndex(p => new { p.FieldSetId, p.Name }).HasName($"IX_{nameof(FieldSet)}_{nameof(Field)}_{nameof(Field.Name)}").IsUnique();
        }
    }
}
