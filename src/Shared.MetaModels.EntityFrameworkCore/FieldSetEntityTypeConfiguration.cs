﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.MetaModels.EntityFrameworkCore
{
    public class FieldSetEntityTypeConfiguration : IEntityTypeConfiguration<FieldSet>
    {
        public void Configure(EntityTypeBuilder<FieldSet> builder)
        {
            builder.Property(p => p.Name).HasMaxLength(70).IsRequired();
            builder.Property(p => p.Description).HasMaxLength(200);

            // Indexes
            builder.HasIndex(p => p.Name).HasName($"IX_{nameof(FieldSet)}_{nameof(FieldSet.Name)}");
        }
    }
}
