﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Shared.MetaModels.EntityFrameworkCore
{
    public class FieldValueEntityTypeConfiguration : IEntityTypeConfiguration<FieldValue>
    {
        public void Configure(EntityTypeBuilder<FieldValue> builder)
        {
            builder.HasKey(p => new { p.EntityId, p.FieldId });
            builder.Property(p => p.LastUpdate).HasDefaultValueSql("getdate()");

            // Relations
            builder.HasOne(p => p.Field)
                .WithMany(p => p.Values)
                .HasForeignKey(p => p.FieldId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(p => p.Entity)
                .WithMany(p => p.Values)
                .HasForeignKey(p => p.EntityId)
                .OnDelete(DeleteBehavior.Restrict);

        }
    }
}
