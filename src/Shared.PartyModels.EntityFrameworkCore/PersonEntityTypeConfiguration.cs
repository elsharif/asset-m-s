﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Shared.PartyModels.EntityFrameworkCore
{
    public class PersonEntityTypeConfiguration : PartyEntityTypeConfiguration<Person>
    {
        public override void Configure(EntityTypeBuilder<Person> builder)
        {
            builder.Property(p => p.FirstName).HasMaxLength(75);
            builder.Property(p => p.MiddleName).HasMaxLength(75);
            builder.Property(p => p.LastName).HasMaxLength(75).IsRequired();

            // Indexes
            builder.HasIndex(p => p.LastName).HasName($"IX_{nameof(Person)}_{nameof(Person.LastName)}");
        }
    }
}
