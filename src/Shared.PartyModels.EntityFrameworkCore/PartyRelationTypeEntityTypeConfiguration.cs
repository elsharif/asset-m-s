﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Shared.PartyModels.EntityFrameworkCore
{
    public class PartyRelationTypeEntityTypeConfiguration : IEntityTypeConfiguration<PartyRelationType>
    {
        public void Configure(EntityTypeBuilder<PartyRelationType> builder)
        {
            builder.Property(p => p.Name).HasMaxLength(75).IsRequired();

            // Indexes
            builder.HasIndex(p => p.Name).HasName($"IX_{nameof(PartyRelationType)}_{nameof(PartyRelationType.Name)}");
        }
    }
}
