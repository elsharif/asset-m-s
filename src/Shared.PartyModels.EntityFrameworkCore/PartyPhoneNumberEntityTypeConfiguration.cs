﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.PartyModels.EntityFrameworkCore
{
    public class PartyPhoneNumberEntityTypeConfiguration : IEntityTypeConfiguration<PartyPhoneNumber>
    {
        public void Configure(EntityTypeBuilder<PartyPhoneNumber> builder)
        {
            builder.OwnsOne(p => p.PhoneNumber, c =>
            {
                c.Property(x => x.Name).HasMaxLength(20).IsRequired();
                c.Property(x => x.Number).HasMaxLength(30).IsRequired();
            });

            builder.HasOne(p => p.Party)
                .WithMany(p => p.PhoneNumbers)
                .HasForeignKey(p => p.PartyId);

            builder.HasIndex(p => new { p.PartyId });
        }
    }
}
