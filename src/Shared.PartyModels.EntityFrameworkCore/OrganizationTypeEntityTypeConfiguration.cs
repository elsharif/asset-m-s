﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Shared.PartyModels.EntityFrameworkCore
{
    public class OrganizationTypeEntityTypeConfiguration : IEntityTypeConfiguration<OrganizationType>
    {
        public void Configure(EntityTypeBuilder<OrganizationType> builder)
        {
            builder.Property(p => p.Name).HasMaxLength(75).IsRequired();

            // Indexes
            builder.HasIndex(p => p.Name).HasName($"IX_{nameof(OrganizationType)}_{nameof(OrganizationType.Name)}").IsUnique();
        }
    }
}
