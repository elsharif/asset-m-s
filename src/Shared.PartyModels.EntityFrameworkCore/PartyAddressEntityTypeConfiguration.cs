﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Shared.PartyModels.EntityFrameworkCore
{
    public class PartyAddressEntityTypeConfiguration : IEntityTypeConfiguration<PartyAddress>
    {
        public void Configure(EntityTypeBuilder<PartyAddress> builder)
        {
            builder.OwnsOne(p => p.Address, c =>
            {
                c.Property(x => x.Name).HasMaxLength(20).IsRequired();
                c.Property(x => x.Country).HasMaxLength(50);
                c.Property(x => x.City).HasMaxLength(50);
                c.Property(x => x.Line1).HasMaxLength(100);
                c.Property(x => x.Line2).HasMaxLength(100);
            });

            builder.HasOne(p => p.Party)
                .WithMany(p => p.Addresses)
                .HasForeignKey(p => p.PartyId);

            builder.HasIndex(p => new { p.PartyId });
        }
    }
}
