﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.Collections.Generic;

namespace Shared.PartyModels.EntityFrameworkCore
{
    public class PartyRelationEntityTypeConfiguration : IEntityTypeConfiguration<PartyRelation>
    {
        public virtual void Configure(EntityTypeBuilder<PartyRelation> builder)
        {
            builder.Property(p => p.FromDate).HasDefaultValueSql("getdate()");

            // Relations
            builder.HasOne(p => p.Type)
                .WithMany(p => p.PartyRelations)
                .HasForeignKey(p => p.TypeId)
                .OnDelete(DeleteBehavior.Restrict);


            // Indexes
            builder.HasIndex(p => p.TypeId).HasName($"IX_{nameof(PartyRelation)}_{nameof(PartyRole.Type)}");
        }
    }
}
