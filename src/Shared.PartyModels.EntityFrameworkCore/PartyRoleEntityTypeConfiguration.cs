﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.Collections.Generic;

namespace Shared.PartyModels.EntityFrameworkCore
{
    public class PartyRoleEntityTypeConfiguration : IEntityTypeConfiguration<PartyRole>
    {
        public virtual void Configure(EntityTypeBuilder<PartyRole> builder)
        {
            builder.Property(p => p.FromDate).HasDefaultValueSql("getdate()");

            // Relations
            builder.HasOne(p => p.Party)
                .WithMany(p => p.PartyRoles)
                .HasForeignKey(p => p.PartyId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(p => p.Type)
                .WithMany(p => p.PartyRoles)
                .HasForeignKey(p => p.TypeId)
                .OnDelete(DeleteBehavior.Restrict);
                

            // Indexes
            builder.HasIndex(p => new { p.PartyId, p.TypeId }).HasName($"IX_{nameof(PartyRole)}_{nameof(PartyRole.Party)}_{nameof(PartyRole.Type)}");
        }
    }
}
