﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Shared.Models;
using System;
using System.Text;

namespace Shared.PartyModels.EntityFrameworkCore
{
    public abstract class PartyEntityTypeConfiguration<T> : IEntityTypeConfiguration<T>
        where T : Party
    {
        public virtual void Configure(EntityTypeBuilder<T> builder)
        {
            builder.Property(p => p.DisplayName).HasMaxLength(75).IsRequired();

            // Indexes
            builder.HasIndex(p => p.DisplayName).HasName($"IX_{nameof(Party)}_{nameof(Party.DisplayName)}");
        }
    }
}
