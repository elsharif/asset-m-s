﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Shared.PartyModels.EntityFrameworkCore
{
    public class PartyEmailAddressEntityTypeConfiguration : IEntityTypeConfiguration<PartyEmailAddress>
    {
        public void Configure(EntityTypeBuilder<PartyEmailAddress> builder)
        {
            builder.OwnsOne(p => p.EmailAddress, c =>
            {
                c.Property(x => x.Name).HasMaxLength(20).IsRequired();
                c.Property(x => x.Value).HasMaxLength(250).IsRequired();
            });

            builder.HasOne(p => p.Party)
                .WithMany(p => p.EmailAddresses)
                .HasForeignKey(p => p.PartyId);

            builder.HasIndex(p => new { p.PartyId });
        }
    }
}
