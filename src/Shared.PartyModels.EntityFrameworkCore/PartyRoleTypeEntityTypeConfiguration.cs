﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Shared.PartyModels.EntityFrameworkCore
{
    public class PartyRoleTypeEntityTypeConfiguration : IEntityTypeConfiguration<PartyRoleType>
    {
        public void Configure(EntityTypeBuilder<PartyRoleType> builder)
        {
            builder.Property(p => p.Name).HasMaxLength(75).IsRequired();

            // Indexes
            builder.HasIndex(p => p.Name).HasName($"IX_{nameof(PartyRoleType)}_{nameof(PartyRoleType.Name)}");
        }
    }
}
