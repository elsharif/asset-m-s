﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.PartyModels.EntityFrameworkCore
{
    public class PartyRoleConfigurationEntityTypeConfiguration : IEntityTypeConfiguration<PartyRoleConfiguration>
    {
        public void Configure(EntityTypeBuilder<PartyRoleConfiguration> builder)
        {
            builder.HasOne(p => p.PartyRole)
                .WithOne(p => p.Configuration)
                .HasForeignKey<PartyRole>(p => p.Id)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
