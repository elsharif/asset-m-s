﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Shared.PartyModels.EntityFrameworkCore
{
    public class OrganizationEntityTypeConfiguration : PartyEntityTypeConfiguration<Organization>
    {
        public override void Configure(EntityTypeBuilder<Organization> builder)
        {
            builder.Property(p => p.Name).HasMaxLength(75).IsRequired();

            // Relations
            builder.HasOne(p => p.Type)
                .WithMany(p => p.Organizations)
                .HasForeignKey(p => p.TypeId)
                .OnDelete(DeleteBehavior.SetNull);

            // Indexes
            builder.HasIndex(p => p.Name).HasName($"IX_{nameof(Organization)}_{nameof(Organization.Name)}");
        }
    }
}
