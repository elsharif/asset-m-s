﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.PartyModels.EntityFrameworkCore
{
    public class PartyClassificationEntityTypeConfiguration : IEntityTypeConfiguration<PartyClassification>
    {
        public void Configure(EntityTypeBuilder<PartyClassification> builder)
        {
            builder.HasKey(p => new { p.PartyId, p.ClassificationId });

            // Relations
            builder.HasOne(p => p.Party)
                .WithMany(p => p.Classifications)
                .HasForeignKey(p => p.PartyId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(p => p.Classification)
                .WithMany()
                .HasForeignKey(p => p.ClassificationId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
