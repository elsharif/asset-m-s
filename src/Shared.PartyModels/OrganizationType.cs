﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.PartyModels
{
    public class OrganizationType
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<Organization> Organizations { get; private set; }
    }
}
