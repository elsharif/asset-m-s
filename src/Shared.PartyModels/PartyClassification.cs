﻿using Shared.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.PartyModels
{
    public class PartyClassification
    {
        public Guid PartyId { get; set; }
        public Party Party { get; set; }
        public Guid ClassificationId { get; set; }
        public ClassificationTypeValue Classification { get; set; }
    }
}
