﻿using Shared.Models;
using System;

namespace Shared.PartyModels
{
    public class PartyEmailAddress
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public Guid PartyId { get; set; }
        public Party Party { get; set; }
        public EmailAddress EmailAddress { get; set; } = new EmailAddress();
    }
}
