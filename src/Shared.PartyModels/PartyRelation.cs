﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.PartyModels
{
    public class PartyRelation
    {
        public Guid Id { get; set; }
        public Guid TypeId { get; set; }
        public PartyRelationType Type { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime? ThruDate { get; set; }
        public ICollection<PartyRelationRole> Roles { get; private set; }
    }
}
