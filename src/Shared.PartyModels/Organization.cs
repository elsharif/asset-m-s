﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.PartyModels
{
    public sealed class Organization : Party
    {
        public string Name { get; set; }
        public Guid? TypeId { get; set; }
        public OrganizationType Type { get; set; }
    }
}
