﻿using Shared.Models;
using System;

namespace Shared.PartyModels
{
    public class PartyAddress
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public Guid PartyId { get; set; }
        public Party Party { get; set; }
        public Address Address { get; set; } = new Address();
    }
}
