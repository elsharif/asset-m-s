﻿using Shared.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.PartyModels
{
    public class PartyPhoneNumber
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public Guid PartyId { get; set; }
        public Party Party { get; set; }
        public PhoneNumber PhoneNumber { get; set; } = new PhoneNumber();
    }
}
