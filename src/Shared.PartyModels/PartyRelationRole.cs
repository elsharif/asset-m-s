﻿using System;

namespace Shared.PartyModels
{
    public class PartyRelationRole
    {
        public Guid Id { get; set; }
        public Guid RelationId { get; set; }
        public PartyRelation Relation { get; set; }
        public Guid RoleId { get; set; }
        public PartyRole Role { get; set; }
        public string RoleName { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime? ThruDate { get; set; }
    }
}
