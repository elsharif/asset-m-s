﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.PartyModels.Enumerations
{
    public enum Gender
    {
        Undefined,
        Male,
        Female
    }
}
