﻿using Shared.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.PartyModels
{
    public abstract class Party
    {
        public Guid Id { get; set; }
        public string DisplayName { get; set; }
        public IEnumerable<PartyRole> PartyRoles { get; private set; }
        public IEnumerable<PartyClassification> Classifications { get; private set; }
        public List<PartyPhoneNumber> PhoneNumbers { get; private set; } = new List<PartyPhoneNumber>();
        public List<PartyEmailAddress> EmailAddresses { get; private set; } = new List<PartyEmailAddress>();
        public List<PartyAddress> Addresses { get; private set; } = new List<PartyAddress>();
    }
}
