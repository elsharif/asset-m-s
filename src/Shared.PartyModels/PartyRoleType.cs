﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.PartyModels
{
    public class PartyRoleType
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<PartyRole> PartyRoles { get; private set; }
    }
}
