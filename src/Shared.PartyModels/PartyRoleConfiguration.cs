﻿using Shared.MetaModels;

namespace Shared.PartyModels
{
    public class PartyRoleConfiguration : ConfigurableEntity
    {
        public PartyRole PartyRole { get; set; }
    }
}
