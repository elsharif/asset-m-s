﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.PartyModels
{
    public class PartyRelationType
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public ICollection<PartyRelation> PartyRelations { get; private set; }
    }
}
