﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.PartyModels
{
    public class PartyRole
    {
        public Guid Id { get; set; }
        public Guid TypeId { get; set; }
        public PartyRoleType Type { get; set; }
        public Guid PartyId { get; set; }
        public Party Party { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime? ThruDate { get; set; }
        public PartyRoleConfiguration Configuration { get; set; } = new PartyRoleConfiguration();
        public ICollection<PartyRelationRole> RelationRoles { get; private set; }
    }
}
