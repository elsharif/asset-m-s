﻿using Shared.PartyModels.Enumerations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.PartyModels
{
    public sealed class Person : Party
    {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public Gender Gender { get; set; }
        public DateTime? Birthdate { get; set; }
    }
}
