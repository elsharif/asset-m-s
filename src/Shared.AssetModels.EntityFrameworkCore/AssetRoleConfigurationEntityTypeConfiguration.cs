﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Shared.AssetModels.EntityFrameworkCore
{
    public class AssetRoleConfigurationEntityTypeConfiguration : IEntityTypeConfiguration<AssetRoleConfiguration>
    {
        public void Configure(EntityTypeBuilder<AssetRoleConfiguration> builder)
        {
            builder.HasOne(p => p.AssetRole)
                .WithOne(p => p.Configuration)
                .HasForeignKey<AssetRole>(p => p.Id)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
