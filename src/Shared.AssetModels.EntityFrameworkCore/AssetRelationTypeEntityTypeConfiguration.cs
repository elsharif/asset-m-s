﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Shared.AssetModels.EntityFrameworkCore
{
    public class AssetRelationTypeEntityTypeConfiguration : IEntityTypeConfiguration<AssetRelationType>
    {
        public void Configure(EntityTypeBuilder<AssetRelationType> builder)
        {
            builder.Property(p => p.Name).HasMaxLength(75).IsRequired();

            // Indexes
            builder.HasIndex(p => p.Name).HasName($"IX_{nameof(AssetRelationType)}_{nameof(AssetRelationType.Name)}");
        }
    }
}
