﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Shared.AssetModels.EntityFrameworkCore
{
    public class AssetPartyRelationRoleEntityTypeConfiguration : IEntityTypeConfiguration<AssetPartyRelationRole>
    {
        public void Configure(EntityTypeBuilder<AssetPartyRelationRole> builder)
        {
            builder.Property(p => p.RoleName).HasMaxLength(70).IsRequired();
            builder.Property(p => p.FromDate).HasDefaultValueSql("getdate()");

            // Relations
            builder.HasOne(p => p.Relation)
                .WithMany(p => p.PartyRoles)
                .HasForeignKey(p => p.RelationId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(p => p.Role)
                .WithMany()
                .HasForeignKey(p => p.RoleId)
                .OnDelete(DeleteBehavior.Restrict);

            // Indexes
            builder.HasIndex(p => new { p.RelationId, p.RoleId, p.RoleName }).HasName($"IX_{nameof(AssetPartyRelationRole)}_{nameof(AssetPartyRelationRole.Relation)}");
            builder.HasIndex(p => p.RoleName).HasName($"IX_{nameof(AssetPartyRelationRole)}_{nameof(AssetPartyRelationRole.RoleName)}");
        }
    }
}
