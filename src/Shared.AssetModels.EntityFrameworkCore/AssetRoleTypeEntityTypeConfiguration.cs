﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Shared.AssetModels.EntityFrameworkCore
{
    public class AssetRoleTypeEntityTypeConfiguration : IEntityTypeConfiguration<AssetRoleType>
    {
        public void Configure(EntityTypeBuilder<AssetRoleType> builder)
        {
            builder.Property(p => p.Name).HasMaxLength(75).IsRequired();

            // Indexes
            builder.HasIndex(p => p.Name).HasName($"IX_{nameof(AssetRoleType)}_{nameof(AssetRoleType.Name)}");
        }
    }
}
