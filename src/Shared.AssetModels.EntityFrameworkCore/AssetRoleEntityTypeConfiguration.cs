﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Shared.AssetModels.EntityFrameworkCore
{
    public class AssetRoleEntityTypeConfiguration : IEntityTypeConfiguration<AssetRole>
    {
        public void Configure(EntityTypeBuilder<AssetRole> builder)
        {
            builder.Property(p => p.FromDate).HasDefaultValueSql("getdate()");

            // Relations
            builder.HasOne(p => p.Asset)
                .WithMany(p => p.AssetRoles)
                .HasForeignKey(p => p.AssetId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(p => p.Type)
                .WithMany(p => p.AssetRoles)
                .HasForeignKey(p => p.TypeId)
                .OnDelete(DeleteBehavior.Restrict);


            // Indexes
            builder.HasIndex(p => new { p.AssetId, p.TypeId }).HasName($"IX_{nameof(AssetRole)}_{nameof(AssetRole.Asset)}_{nameof(AssetRole.Type)}");
        }
    }
}
