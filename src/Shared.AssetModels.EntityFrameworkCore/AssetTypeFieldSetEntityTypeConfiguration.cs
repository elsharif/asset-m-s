﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Shared.AssetModels.EntityFrameworkCore
{
    public class AssetTypeFieldSetEntityTypeConfiguration : IEntityTypeConfiguration<AssetTypeFieldSet>
    {
        public void Configure(EntityTypeBuilder<AssetTypeFieldSet> builder)
        {
            builder.HasKey(p => new { p.TypeId, p.FieldSetId });

            // Relation
            builder.HasOne(p => p.Type)
                .WithMany(p => p.FieldSets)
                .HasForeignKey(p => p.TypeId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(p => p.FieldSet)
                .WithMany()
                .HasForeignKey(p => p.FieldSetId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
