﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.AssetModels.EntityFrameworkCore
{
    public class AssetEntityTypeConfiguration : IEntityTypeConfiguration<Asset>
    {
        public void Configure(EntityTypeBuilder<Asset> builder)
        {
            builder.Property(p => p.Name).HasMaxLength(70).IsRequired();

            builder.HasOne(p => p.Type)
                .WithMany(p => p.Assets)
                .HasForeignKey(p => p.TypeId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasIndex(p => p.Name).HasName($"IX_{nameof(Asset)}_{nameof(Asset.Name)}");
        }
    }
}
