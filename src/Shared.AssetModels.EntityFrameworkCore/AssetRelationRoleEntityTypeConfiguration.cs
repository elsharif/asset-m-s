﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Shared.AssetModels.EntityFrameworkCore
{
    public class AssetRelationRoleEntityTypeConfiguration : IEntityTypeConfiguration<AssetRelationRole>
    {
        public void Configure(EntityTypeBuilder<AssetRelationRole> builder)
        {
            builder.Property(p => p.RoleName).HasMaxLength(70).IsRequired();
            builder.Property(p => p.FromDate).HasDefaultValueSql("getdate()");

            // Relations
            builder.HasOne(p => p.Relation)
                .WithMany(p => p.Roles)
                .HasForeignKey(p => p.RelationId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(p => p.Role)
                .WithMany(p => p.RelationRoles)
                .HasForeignKey(p => p.RoleId)
                .OnDelete(DeleteBehavior.Restrict);

            // Indexes
            builder.HasIndex(p => new { p.RelationId, p.RoleId, p.RoleName }).HasName($"IX_{nameof(AssetRelationRole)}_{nameof(AssetRelationRole.Relation)}");
            builder.HasIndex(p => p.RoleName).HasName($"IX_{nameof(AssetRelationRole)}_{nameof(AssetRelationRole.RoleName)}");
        }
    }
}
