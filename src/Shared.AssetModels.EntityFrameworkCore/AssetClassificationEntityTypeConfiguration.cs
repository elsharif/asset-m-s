﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Shared.AssetModels.EntityFrameworkCore
{
    public class AssetClassificationEntityTypeConfiguration : IEntityTypeConfiguration<AssetClassification>
    {
        public void Configure(EntityTypeBuilder<AssetClassification> builder)
        {
            builder.HasKey(p => new { p.AssetId, p.ClassificationId });

            // Relations
            builder.HasOne(p => p.Asset)
                .WithMany(p => p.Classifications)
                .HasForeignKey(p => p.AssetId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(p => p.Classification)
                .WithMany()
                .HasForeignKey(p => p.ClassificationId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
