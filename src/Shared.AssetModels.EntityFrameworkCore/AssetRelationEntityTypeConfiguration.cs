﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Shared.AssetModels.EntityFrameworkCore
{
    public class AssetRelationEntityTypeConfiguration : IEntityTypeConfiguration<AssetRelation>
    {
        public void Configure(EntityTypeBuilder<AssetRelation> builder)
        {
            builder.Property(p => p.FromDate).HasDefaultValueSql("getdate()");

            // Relations
            builder.HasOne(p => p.Type)
                .WithMany(p => p.AssetRelations)
                .HasForeignKey(p => p.TypeId)
                .OnDelete(DeleteBehavior.Restrict);


            // Indexes
            builder.HasIndex(p => p.TypeId).HasName($"IX_{nameof(AssetRelation)}_{nameof(AssetRole.Type)}");
        }
    }
}
