﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AMS.Infrastructure;
using Shared.DocumentModels;
using Microsoft.AspNetCore.Http;
using System.IO;

namespace WebApplication1.Controllers
{
    public class DocumentsController : Controller
    {
        private readonly DomainDbContext _context;

        public DocumentsController(DomainDbContext context)
        {
            _context = context;
        }

        // GET: Documents
        public async Task<IActionResult> Index(Guid repositoryId, Guid? folderId = null)
        {
            var domainDbContext = _context.Documents.Include(d => d.Folder).Include(d => d.Repository)
                .Where(x => x.RepositoryId == repositoryId 
                    && (!folderId.HasValue || x.FolderId == folderId)
                );
            return View(await domainDbContext.ToListAsync());
        }

        // GET: Documents/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var document = await _context.Documents
                .Include(d => d.Folder)
                .Include(d => d.Repository)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (document == null)
            {
                return NotFound();
            }

            return View(document);
        }

        // GET: Documents/Create
        public IActionResult Create()
        {
            ViewData["FolderId"] = new SelectList(_context.Folders, "Id", "Title");
            ViewData["RepositoryId"] = new SelectList(_context.Repositories, "Id", "Name");

            return View();
        }

        // POST: Documents/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,RepositoryId,FolderId,Title,CreationTime,LastModificationTime,Comments")] Document document, IFormFile file)
        {
            if (ModelState.IsValid)
            {
                document.Id = Guid.NewGuid();
               
                var docFile = new DocumentFile()
                {
                    Id = Guid.NewGuid(),
                    Document = document,
                    FileName = file.FileName,
                    FileSize = file.Length,
                    FileCreationTime = DateTime.Now,
                    FileLastModificationTime = DateTime.Now
                };
                var chunk = new FileChunk()
                {
                    Id = Guid.NewGuid(),
                    DocumentFile = docFile,
                    SNO = 1
                };
                using (var memoryStream = new MemoryStream())
                {
                    await file.CopyToAsync(memoryStream);
                    chunk.Contents = memoryStream.ToArray();
                }
                docFile.Chunks.Add(chunk);
                document.Files.Add(docFile);

                _context.Add(document);

                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["FolderId"] = new SelectList(_context.Folders, "Id", "Title", document.FolderId);
            ViewData["RepositoryId"] = new SelectList(_context.Repositories, "Id", "Name", document.RepositoryId);
            return View(document);
        }

        // GET: Documents/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var document = await _context.Documents.FindAsync(id);
            if (document == null)
            {
                return NotFound();
            }
            ViewData["FolderId"] = new SelectList(_context.Folders, "Id", "Title", document.FolderId);
            ViewData["RepositoryId"] = new SelectList(_context.Repositories, "Id", "Name", document.RepositoryId);
            return View(document);
        }

        // POST: Documents/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Id,RepositoryId,FolderId,Title,CreationTime,LastModificationTime,Comments")] Document document)
        {
            if (id != document.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(document);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DocumentExists(document.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["FolderId"] = new SelectList(_context.Folders, "Id", "Title", document.FolderId);
            ViewData["RepositoryId"] = new SelectList(_context.Repositories, "Id", "Name", document.RepositoryId);
            return View(document);
        }

        // GET: Documents/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var document = await _context.Documents
                .Include(d => d.Folder)
                .Include(d => d.Repository)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (document == null)
            {
                return NotFound();
            }

            return View(document);
        }

        // POST: Documents/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var document = await _context.Documents.FindAsync(id);
            _context.Documents.Remove(document);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DocumentExists(Guid id)
        {
            return _context.Documents.Any(e => e.Id == id);
        }

        public IActionResult Download(Guid id)
        {
            var docFile = _context.DocumentFiles.Include(x => x.Chunks).Where(x => x.DocumentId == id).FirstOrDefault();
            var chunk = docFile.Chunks.FirstOrDefault();
            var mem = new MemoryStream(chunk.Contents);
            return File(mem, "application/octet-stream", docFile.FileName);
        }
    }
}
