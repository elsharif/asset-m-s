﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AMS.Infrastructure;
using AMS.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using Shared.AssetModels;

namespace WebApplication1.Controllers
{
    public class AssetTypesController : Controller
    {
        private readonly DomainDbContext context;

        public AssetTypesController(DomainDbContext context)
        {
            this.context = context;
        }

        public async Task<ActionResult> Index()
        {
            return View(await context.AssetTypes.ToListAsync());
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(AssetType assetType)
        {
            try
            {
                context.AssetTypes.Add(assetType);
                await context.SaveChangesAsync();

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        public async Task<ActionResult> Edit(Guid id)
        {
            return View(await context.AssetTypes.FindAsync(id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Guid id, AssetType assetType)
        {
            try
            {
                context.AssetTypes.Update(assetType);
                await context.SaveChangesAsync();

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

    }
}