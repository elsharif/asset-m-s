﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AMS.Infrastructure;
using Shared.DocumentModels;

namespace WebApplication1.Controllers
{
    public class FoldersController : Controller
    {
        private readonly DomainDbContext _context;

        public FoldersController(DomainDbContext context)
        {
            _context = context;
        }

        // GET: Folders
        public async Task<IActionResult> Index(Guid repositoryId, Guid? parentId = null)
        {
            var domainDbContext = _context.Folders.Include(f => f.Parent).Include(f => f.Repository)
                .Where(x => x.RepositoryId == repositoryId
                    && ((!parentId.HasValue && !x.ParentId.HasValue) || (parentId.HasValue && x.ParentId == parentId))
                );
            return View(await domainDbContext.ToListAsync());
        }

        // GET: Folders/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var folder = await _context.Folders
                .Include(f => f.Parent)
                .Include(f => f.Repository)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (folder == null)
            {
                return NotFound();
            }

            return View(folder);
        }

        // GET: Folders/Create
        public IActionResult Create()
        {
            ViewData["ParentId"] = new SelectList(_context.Folders, "Id", "Title");
            ViewData["RepositoryId"] = new SelectList(_context.Repositories, "Id", "Name");
            return View();
        }

        // POST: Folders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,RepositoryId,ParentId,Title,Comments")] Folder folder)
        {
            if (ModelState.IsValid)
            {
                folder.Id = Guid.NewGuid();
                _context.Add(folder);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ParentId"] = new SelectList(_context.Folders, "Id", "Title", folder.ParentId);
            ViewData["RepositoryId"] = new SelectList(_context.Repositories, "Id", "Name", folder.RepositoryId);
            return View(folder);
        }

        // GET: Folders/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var folder = await _context.Folders.FindAsync(id);
            if (folder == null)
            {
                return NotFound();
            }
            ViewData["ParentId"] = new SelectList(_context.Folders, "Id", "Title", folder.ParentId);
            ViewData["RepositoryId"] = new SelectList(_context.Repositories, "Id", "Name", folder.RepositoryId);
            return View(folder);
        }

        // POST: Folders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Id,RepositoryId,ParentId,Title,Comments")] Folder folder)
        {
            if (id != folder.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(folder);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FolderExists(folder.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ParentId"] = new SelectList(_context.Folders, "Id", "Title", folder.ParentId);
            ViewData["RepositoryId"] = new SelectList(_context.Repositories, "Id", "Name", folder.RepositoryId);
            return View(folder);
        }

        // GET: Folders/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var folder = await _context.Folders
                .Include(f => f.Parent)
                .Include(f => f.Repository)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (folder == null)
            {
                return NotFound();
            }

            return View(folder);
        }

        // POST: Folders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var folder = await _context.Folders.FindAsync(id);
            _context.Folders.Remove(folder);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool FolderExists(Guid id)
        {
            return _context.Folders.Any(e => e.Id == id);
        }
    }
}
