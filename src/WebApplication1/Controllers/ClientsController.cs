﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AMS.Infrastructure;
using AMS.Infrastructure.Models;

namespace WebApplication1.Controllers
{
    public class ClientsController : Controller
    {
        private readonly DomainDbContext context;

        public ClientsController(DomainDbContext context)
        {
            this.context = context;
        }

        // GET: Clients
        public async Task<ActionResult> Index()
        {
            return View(await context.GetClients());
        }

        // GET: Clients/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Clients/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Client client)
        {
            try
            {
                await context.AddClient(client);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Clients/Edit/5
        public async Task<ActionResult> Edit(Guid id)
        {
            return View(await context.GetClientById(id));
        }

        // POST: Clients/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Guid id, Client client)
        {
            try
            {
                await context.UpdateClient(client);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
        
    }
}