﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AMS.Infrastructure;
using AMS.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebApplication1.Controllers
{
    public class AssetsController : Controller
    {
        private readonly DomainDbContext context;

        public AssetsController(DomainDbContext context)
        {
            this.context = context;
        }

        public async Task<ActionResult> Index(Guid clientId, string clientName)
        {
            ViewBag.ClientId = clientId;
            ViewBag.ClientName = clientName;
            return View(await context.GetAssets(clientId));
        }

        public async Task<ActionResult> Create(Guid clientId, string clientName)
        {
            var assetTypes = await context.AssetTypes.Select(x => new { Id = x.Id, Name = x.Name }).ToListAsync();
            ViewBag.AssetTypes = new SelectList(assetTypes, "Id", "Name");
            ViewBag.ClientId = clientId;
            ViewBag.ClientName = clientName;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(DefaultAsset asset)
        {
            try
            {
                await context.AddAsset(asset);

                return RedirectToAction(nameof(Index), new { clientId = asset.Client.Id });
            }
            catch(Exception ex)
            {
                ;
                return View();
            }
        }

        public async Task<ActionResult> Edit(Guid id, Guid clientId)
        {
            var assetTypes = await context.AssetTypes.Select(x => new { Id = x.Id, Name = x.Name }).ToListAsync();
            ViewBag.AssetTypes = new SelectList(assetTypes, "Id", "Name");
            DefaultAsset model = await context.GetAssetById(id, clientId);
            ViewBag.ClientId = clientId;
            ViewBag.ClientName = model.Client.Name;

            return base.View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Guid id, DefaultAsset asset)
        {
            try
            {
                await context.UpdateAsset(asset);

                return RedirectToAction(nameof(Index), new { clientId = asset.Client.Id });
            }
            catch
            {
                return View();
            }
        }

    }
}