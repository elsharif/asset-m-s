﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AMS.Infrastructure;
using Shared.DocumentModels;

namespace WebApplication1.Controllers
{
    public class RepositoriesController : Controller
    {
        private readonly DomainDbContext _context;

        public RepositoriesController(DomainDbContext context)
        {
            _context = context;
        }

        // GET: Repositories
        public async Task<IActionResult> Index()
        {
            return View(await _context.Repositories.ToListAsync());
        }

        // GET: Repositories/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var repository = await _context.Repositories
                .FirstOrDefaultAsync(m => m.Id == id);
            if (repository == null)
            {
                return NotFound();
            }

            return View(repository);
        }

        // GET: Repositories/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Repositories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name")] Repository repository)
        {
            if (ModelState.IsValid)
            {
                repository.Id = Guid.NewGuid();
                _context.Add(repository);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(repository);
        }

        // GET: Repositories/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var repository = await _context.Repositories.FindAsync(id);
            if (repository == null)
            {
                return NotFound();
            }
            return View(repository);
        }

        // POST: Repositories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Id,Name")] Repository repository)
        {
            if (id != repository.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(repository);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RepositoryExists(repository.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(repository);
        }

        // GET: Repositories/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var repository = await _context.Repositories
                .FirstOrDefaultAsync(m => m.Id == id);
            if (repository == null)
            {
                return NotFound();
            }

            return View(repository);
        }

        // POST: Repositories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var repository = await _context.Repositories.FindAsync(id);
            _context.Repositories.Remove(repository);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool RepositoryExists(Guid id)
        {
            return _context.Repositories.Any(e => e.Id == id);
        }
    }
}
