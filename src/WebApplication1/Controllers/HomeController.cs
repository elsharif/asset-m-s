﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using AMS.Infrastructure;
using AMS.Infrastructure.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        private readonly DomainDbContext context;

        public HomeController(DomainDbContext context)
        {
            this.context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Test()
        {
            try
            {
                var at = await context.AssetTypes.FirstOrDefaultAsync();
                for (int i = 1; i < 20; i++)
                {
                    var client = new Client { Name = $"Client {i}" };
                    await context.AddClient(client);
                    for (int j = 0; j < 29; j++)
                    {
                        var asset = new DefaultAsset { Client = client, Type = at, Name = $"Asset - {client.Name} - {j}", Description = "Test" };
                        await context.AddAsset(asset);
                    }
                }
               
            }
            catch
            {

            }

            return RedirectToAction("Index", "Clients");
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
