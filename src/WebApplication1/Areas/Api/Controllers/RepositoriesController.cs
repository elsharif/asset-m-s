﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AMS.Infrastructure;
using Shared.DocumentModels;

namespace WebApplication1.Areas.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RepositoriesController : ControllerBase
    {
        private readonly DomainDbContext _context;

        public RepositoriesController(DomainDbContext context)
        {
            _context = context;
        }

        // GET: api/Repositories
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Repository>>> GetRepositories()
        {
            return await _context.Repositories.ToListAsync();
        }

        // GET: api/Repositories/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Repository>> GetRepository(Guid id)
        {
            var repository = await _context.Repositories.FindAsync(id);

            if (repository == null)
            {
                return NotFound();
            }

            return repository;
        }

        // PUT: api/Repositories/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRepository(Guid id, Repository repository)
        {
            if (id != repository.Id)
            {
                return BadRequest();
            }

            _context.Entry(repository).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RepositoryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Repositories
        [HttpPost]
        public async Task<ActionResult<Repository>> PostRepository(Repository repository)
        {
            _context.Repositories.Add(repository);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetRepository", new { id = repository.Id }, repository);
        }

        // DELETE: api/Repositories/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Repository>> DeleteRepository(Guid id)
        {
            var repository = await _context.Repositories.FindAsync(id);
            if (repository == null)
            {
                return NotFound();
            }

            _context.Repositories.Remove(repository);
            await _context.SaveChangesAsync();

            return repository;
        }

        private bool RepositoryExists(Guid id)
        {
            return _context.Repositories.Any(e => e.Id == id);
        }
    }
}
