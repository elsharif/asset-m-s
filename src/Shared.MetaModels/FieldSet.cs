﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.MetaModels
{
    public class FieldSet
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<EntityFieldSet> Entities { get; private set; } = new List<EntityFieldSet>();
        public List<Field> Fields { get; private set; } = new List<Field>();
    }
}
