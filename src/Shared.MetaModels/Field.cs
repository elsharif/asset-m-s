﻿using Shared.MetaModels.Enumerations;
using System;
using System.Collections.Generic;

namespace Shared.MetaModels
{
    public class Field
    {
        public Guid Id { get; set; }
        public Guid FieldSetId { get; set; }
        public FieldSet FieldSet { get; set; }
        public string Name { get; set; }
        public FieldType Type { get; set; }
        public bool IsRequired { get; set; }
        public string DefaultValue { get; set; }
        public IEnumerable<FieldValue> Values { get; private set; }
    }
}
