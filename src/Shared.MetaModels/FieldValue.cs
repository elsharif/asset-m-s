﻿using System;

namespace Shared.MetaModels
{
    public class FieldValue
    {
        public Guid EntityId { get; set; }
        public ConfigurableEntity Entity { get; set; }
        public Guid FieldId { get; set; }
        public Field Field { get; set; }
        public string Value { get; set; }
        public DateTime LastUpdate { get; set; }
    }
}
