﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.MetaModels
{
    public class ConfigurableEntity
    {
        public Guid Id { get; set; }
        public List<EntityFieldSet> FieldSets { get; private set; } = new List<EntityFieldSet>();
        public List<FieldValue> Values { get; private set; }
    }


}
