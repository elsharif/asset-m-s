﻿using System;

namespace Shared.MetaModels
{
    public class EntityFieldSet
    {
        public Guid EntityId { get; set; }
        public ConfigurableEntity Entity { get; set; }
        public Guid FieldSetId { get; set; }
        public FieldSet FieldSet { get; set; }
    }


}
