﻿using System;
using System.Collections.Generic;

namespace Shared.DocumentModels
{
    public class Document
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public Guid RepositoryId { get; set; }
        public Repository Repository { get; set; }
        public Guid FolderId { get; set; }
        public Folder Folder { get; set; }
        public string Title { get; set; }
        public DateTime CreationTime { get; set; }
        public DateTime LastModificationTime { get; set; }
        public string Comments { get; set; }
        public List<DocumentFile> Files { get; private set; } = new List<DocumentFile>();
    }
}
