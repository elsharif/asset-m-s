﻿using System;
using System.Collections.Generic;

namespace Shared.DocumentModels
{
    public class Folder
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public Guid RepositoryId { get; set; }
        public Repository Repository { get; set; }
        public Guid? ParentId { get; set; }
        public Folder Parent { get; set; }
        public string Title { get; set; }
        public string Comments { get; set; }
        public List<Folder> Childs { get; private set; } = new List<Folder>();
        public List<Document> Documents { get; private set; } = new List<Document>();
    }
}
