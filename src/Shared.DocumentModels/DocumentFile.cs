﻿using System;
using System.Collections.Generic;

namespace Shared.DocumentModels
{
    public class DocumentFile
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public Guid DocumentId { get; set; }
        public Document Document { get; set; }
        public int Version { get; set; }
        public string FileName { get; set; }
        public DateTime FileCreationTime { get; set; }
        public DateTime FileLastModificationTime { get; set; }
        public long FileSize { get; set; }
        public List<FileChunk> Chunks { get; private set; } = new List<FileChunk>();
    }
}
