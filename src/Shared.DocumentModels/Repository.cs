﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.DocumentModels
{
    public class Repository
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public string Name { get; set; }
        public List<Document> Documents { get; private set; } = new List<Document>();
        public List<Folder> Folders { get; private set; } = new List<Folder>();
    }
}
