﻿using System;

namespace Shared.DocumentModels
{
    public class FileChunk
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public Guid DocumentFileId { get; set; }
        public DocumentFile DocumentFile { get; set; }
        public int SNO { get; set; }
        public byte[] Contents { get; set; }
    }
}
