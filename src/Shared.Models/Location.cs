﻿using System;

namespace Shared.Models
{
    public class Location
    {
        public Guid Id { get; set; }
        public Guid TypeId { get; set; }
        public LocationType Type { get; set; }
        public string Name { get; set; }
    }
}
