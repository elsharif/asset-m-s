﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.Models
{
    public class LocationType
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public ICollection<Location> Locations { get; private set; }
    }
}
