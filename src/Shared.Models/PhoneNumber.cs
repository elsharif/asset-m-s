﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.Models
{
    public class PhoneNumber
    {
        public string Name { get; set; }
        public string Number { get; set; }
    }
}
