﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.Models
{
    public class ClassificationType
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public List<ClassificationTypeValue> Values { get; private set; } = new List<ClassificationTypeValue>();
    }
}
