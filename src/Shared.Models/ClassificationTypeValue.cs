﻿using System;

namespace Shared.Models
{
    public class ClassificationTypeValue
    {
        public Guid Id { get; set; }
        public Guid TypeId { get; set; }
        public ClassificationType Type { get; set; }
        public string Value { get; set; }
    }
}
