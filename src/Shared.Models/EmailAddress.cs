﻿namespace Shared.Models
{
    public class EmailAddress
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
