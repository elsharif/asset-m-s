﻿using System;
using System.Collections.Generic;

namespace Shared.AssetModels
{
    public class AssetRelation
    {
        public Guid Id { get; set; }
        public Guid TypeId { get; set; }
        public AssetRelationType Type { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime? ThruDate { get; set; }
        public ICollection<AssetRelationRole> Roles { get; private set; } = new List<AssetRelationRole>();
        public ICollection<AssetPartyRelationRole> PartyRoles { get; private set; } = new List<AssetPartyRelationRole>();
    }
}