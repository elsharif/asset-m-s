﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.AssetModels
{
    public class AssetRelationType
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public ICollection<AssetRelation> AssetRelations { get; private set; }
    }
}
