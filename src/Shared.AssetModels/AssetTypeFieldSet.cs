﻿using Shared.MetaModels;
using System;

namespace Shared.AssetModels
{
    public class AssetTypeFieldSet
    {
        public Guid TypeId { get; set; }
        public AssetType Type { get; set; }
        public Guid FieldSetId { get; set; }
        public FieldSet FieldSet { get; set; }
    }
}
