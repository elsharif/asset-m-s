﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.AssetModels
{
    public class Asset
    {
        public Guid Id { get; set; }
        public Guid TypeId { get; set; }
        public AssetType Type { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public IEnumerable<AssetRole> AssetRoles { get; private set; }
        public IEnumerable<AssetClassification> Classifications { get; private set; }
    }
}
