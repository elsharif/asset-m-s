﻿using System;

namespace Shared.AssetModels
{
    public class AssetRelationRole
    {
        public Guid Id { get; set; }
        public Guid RelationId { get; set; }
        public AssetRelation Relation { get; set; }
        public Guid RoleId { get; set; }
        public AssetRole Role { get; set; }
        public string RoleName { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime? ThruDate { get; set; }
    }
}