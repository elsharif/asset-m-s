﻿using System;
using System.Collections.Generic;

namespace Shared.AssetModels
{
    public class AssetRoleType
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<AssetRole> AssetRoles { get; private set; }
    }
}
