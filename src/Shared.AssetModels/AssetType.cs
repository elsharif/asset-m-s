﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.AssetModels
{
    public class AssetType
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public List<AssetTypeFieldSet> FieldSets { get; private set; } = new List<AssetTypeFieldSet>();
        public IEnumerable<Asset> Assets { get; private set; }
    }
}
