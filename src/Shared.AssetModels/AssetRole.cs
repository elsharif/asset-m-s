﻿using System;
using System.Collections.Generic;

namespace Shared.AssetModels
{
    public class AssetRole
    {
        public Guid Id { get; set; }
        public Guid TypeId { get; set; }
        public AssetRoleType Type { get; set; }
        public Guid AssetId { get; set; }
        public Asset Asset { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime? ThruDate { get; set; }
        public AssetRoleConfiguration Configuration { get; set; } = new AssetRoleConfiguration();
        public ICollection<AssetRelationRole> RelationRoles { get; private set; }
    }
}
