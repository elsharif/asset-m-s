﻿using Shared.PartyModels;
using System;

namespace Shared.AssetModels
{
    public class AssetPartyRelationRole
    {
        public Guid Id { get; set; }
        public Guid RelationId { get; set; }
        public AssetRelation Relation { get; set; }
        public Guid RoleId { get; set; }
        public PartyRole Role { get; set; }
        public string RoleName { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime? ThruDate { get; set; }
    }
}