﻿using Shared.MetaModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.AssetModels
{
    public class AssetRoleConfiguration : ConfigurableEntity
    {
        public AssetRole AssetRole { get; set; }
    }
}
