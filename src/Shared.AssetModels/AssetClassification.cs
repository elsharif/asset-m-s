﻿using Shared.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.AssetModels
{
    public class AssetClassification
    {
        public Guid AssetId { get; set; }
        public Asset Asset { get; set; }
        public Guid ClassificationId { get; set; }
        public ClassificationTypeValue Classification { get; set; }
    }
}
